//
//  HomeViewBuilder.swift
//  viper-basic-slider
//
//  Created by bekbossynova on 8/4/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

protocol HomeView: class {
    func loadCurrentColor(rgb: (CGFloat, CGFloat, CGFloat)) -> (Void)
}

protocol HomeUseCase :class {
    
    func loadCurrentColor() -> (CGFloat, CGFloat, CGFloat)
    func saveCurrentColor(rgb: (CGFloat, CGFloat, CGFloat)) -> ()
    
}

class HomeInteractor: HomeUseCase {
    
    var appColorDao : AppColorDao?
    init(dao: AppColorDao = AppColorDao()) {
        appColorDao = dao
    }
    func loadCurrentColor() -> (CGFloat, CGFloat, CGFloat)  {
        let currentRgb = appColorDao?.fetch()
        return currentRgb!
    }
    
    func saveCurrentColor(rgb: (CGFloat, CGFloat, CGFloat)) -> (Void) {
        appColorDao?.save(rgb: rgb)
    }
    
}

protocol HomeViewWireframe: class {
    var viewcontroller: UIViewController? { get }
}

class HomeViewRouter:  HomeViewWireframe {
    var viewcontroller: UIViewController?
}


protocol HomeViewPresentation: class {
    var view: HomeView?              { get }
    var router: HomeViewWireframe?   { get}
    var interactor: HomeUseCase?     { get}
    
    func onLoadCurrentColor() -> ()
    func onColorValueChange (rgb: (CGFloat, CGFloat, CGFloat)) -> ()
    
}

class  HomeViewPresenter : HomeViewPresentation {
    
    
    weak var view: HomeView?
    var router: HomeViewWireframe?
    var interactor: HomeUseCase?
    
    func onLoadCurrentColor() {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return}
            
            let currentRgb = self.interactor?.loadCurrentColor()
            
            DispatchQueue.main.async {
                
                self.view?.loadCurrentColor(rgb: currentRgb!)
            }
        }
    }
    
    func onColorValueChange(rgb: (CGFloat, CGFloat, CGFloat)) -> Void {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return}
            self.interactor?.saveCurrentColor(rgb: rgb)
        }
    }
    
}


class HomeViewBuilder {
    static func assembleModule() -> UIViewController? {
        let storyboard       = UIStoryboard(name: "Main", bundle: nil)
        let view             = storyboard.instantiateViewController(identifier: "HomeViewController") as? HomeViewController
        let presenter        = HomeViewPresenter()
        let interactor       = HomeInteractor()
        let router           = HomeViewRouter()
        
        view?.presenter       = presenter
        presenter.view        = view
        presenter.interactor  = interactor
        presenter.router      = router
        router.viewcontroller = view
        
        
        return view
    }
    
}
